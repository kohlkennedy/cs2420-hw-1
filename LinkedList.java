public class LinkedList {

	private Node head, tail;
	private int count;

	class Node {
		Board data;
		Node next;
		int count;

		Node(Board d) {
			data = d;
			next = null;
			count = 0;
		}
	}

	public LinkedList() {
		count = 0;
		head = tail = null;
	}

	public Board peek() {
		if (isEmpty())
			return null;
		else
			return head.data;
	}

	public void enqueue(Board elem) {
		Node new_node = new Node(elem);
		if (isEmpty()) {
			head = new_node;
			tail = new_node;
		} else {
			tail.next = new_node;
			tail = new_node;
		}
		count++;
	}

	public Board dequeue() throws Exception {
		if (isEmpty())
			throw new Exception("This list is empty from dequeuing");
		Board temporary = head.data;
		head = head.next;
		count--;
		return temporary;
	}

	public int size() {
		if (isEmpty())
			return 0;
		else {
			return count;
		}
	}

	public boolean isEmpty() {
		return head == null;
	}

	public boolean contains(Board b) {
		if (isEmpty())
			return false;
		Node current = new Node(head.data);
		while (current != tail && current != null) {
			if (current.data == b)
				return true;
			else
				current = current.next;
		}
		return false;
	}
}
