import java.util.Scanner;

public class Tester extends Solver {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int exit = 1;
		do {
			int choice = 0;
			System.out.println("Hello to the board solver.");
			System.out.println("Would you prefer to: \n\n 1. Create a random board \t or \t 2. Create your own?");
			choice = scan.nextInt();
			while (choice != 1 && choice != 2) {
				System.out.println("My apologies, but your input isn't recognized");
				System.out.println("Would you prefer to \n 1. Create a random board \t or \t 2.Create your own?");
				choice = scan.nextInt();
			}
			switch (choice) {
			case 1:
				Board b = new Board();
				System.out.println("How many times would you like the board jumbled?");
				int jumble = scan.nextInt();
				b.makeBoard(jumble);
				System.out.println(b.toString());
				boardSolver(b);
				break;
			case 2:
				Board a = new Board();

				int[] values = new int[9];
				System.out.println("Please enter 9 values for your board");
				for (int i = 0; i < 9; i++)
					values[i] = scan.nextInt();

				for (int i = 0; i < 9; i++)
					System.out.println(values[i]);
				a.makeBoard(values);
				System.out.println("Now the program will solve your board, hopefully");
				scan.nextLine();
				boardSolver(a);
				break;
			}

			System.out.println(
					"Thank you for playing, would you like to play again? \nType 0 to Exit, anything else to continue");
			exit = scan.nextInt();
		} while (exit != 0);
		scan.close();
	}
}
