public class Solver extends Board {

	public static void boardSolver(Board b) {
		boolean solved = false; //The boolean for the while loop
		Board c = new Board(); //Creates the puzzle that will be later used
		int moveCount = 0; //Keeps track of the moves used
		c.makeBoard(0); //Initializes the board, so that it's already solved. It'll be used to compare if the current board is solved or not
		System.out.print(b.toString()); //Prints out the current puzzle
		LinkedList q = new LinkedList(); //initializes the linked list that'll be used
		q.enqueue(b); //Adds the puzzle that needs to be solved to the queue
		System.out.println("This is the current size of the queue " + q.size());
		while (!solved) { //While solved is false, the loop will keep going
			System.out.println(q.peek().toString() + "\n"); //Prints out the current move about to be used
			if (q.peek().equals(c)) { //If the current board equals the solved board, enters the if statement
				System.out.println("The board is solved!!!\nIt took this many moves to solve it: " + moveCount); // States that the board is solved and how many moves it'll take 
				solved = true; // Sets solved to true, thus exiting the loop
			}

			else if (moveCount == 10000000) { //This 
				System.out.println("The size in the queue is " + q.size());
				System.out.println("Sorry Bud, but it\"s not solvable");
				solved = true;
			} else {

				try {
					b = q.dequeue();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if ((b.getLastMove() != 'D') && b.canSlideUp()) {
					Board up = new Board(b);
					up.moveUp();
					//if (!q.contains(up))
						q.enqueue(up);
				}
				if ((b.getLastMove() != 'U') && b.canSlideDown()) {
					Board down = new Board(b);
					down.moveDown();
					//if (!q.contains(down))
						q.enqueue(down);
				}
				if ((b.getLastMove() != 'R') && b.canSlideLeft()) {
					Board left = new Board(b);
					left.moveLeft();
					//if (!q.contains(left))
						q.enqueue(left);
				}
				if ((b.getLastMove() != 'L') && b.canSlideRight()) {
					Board right = new Board(b);
					right.moveRight();
					//if (!q.contains(right))
						q.enqueue(right);
				}
				moveCount++;
			}
		}
	}
}
